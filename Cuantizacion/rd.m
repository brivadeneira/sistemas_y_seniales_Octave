function [xr, er, SNRr] = rd(x, delta)
#Cuantización por redondeo.
#Devuelve y grafica la señal cuantizada y el error de cuantización.
#Requiere a la señal muestreada (x) y tamaño del escalón de cuantización.
xr=(round(x./delta)).*delta;
er=abs(x-xr);
SNRr=(sum(x.^2))/(sum(er.^2));
