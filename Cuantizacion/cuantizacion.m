#Cuantización por redondeo y truncamiento.
#Devuelve y grafica la señal cuantizada y el error de cuantización.
#Requiere a la señal muestreada (x) y el número total de niveles de cuantización (L).
x=input('Ingrese la señal a cuantizar: ');
L=input('Ingrese el número total de niveles de cuantización: ');
delta=(max(x)-min(x))/(L-1);
[xr, er, SNRr] = rd(x, delta);
[xt, et, SNRt] = tr(x, delta);
subplot(2,1,1); stem(x, 'k'); xlabel('n'); ylabel('negro x[n], azul xr[n], rojo xt[n]');
hold on; stem(xr, 'b'); stem(xt, 'r');
subplot(2,1,2); stem(er); xlabel('n'); ylabel('azul er[n], rojo et[n]');
hold on; stem(et, 'r');
disp('SNRr: '); disp(SNRr);
disp('SNRt: '); disp(SNRt);
