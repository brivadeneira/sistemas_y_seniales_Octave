################################################
# Laboratorio 5 - Sistemas y Señales II - 2016 #
################################################
# INSTALACIONES PREVIAS
# sudo apt-get install liboctave-dev
# sudo apt-get install python-symp
#
# Programa principal
# ¡Cuidado! No se implementó control de variables de entrada.
disp('MUESTREO');
xo=@(t) input('Ingrese la señal de entrada: '); syms t; xx=formula(x(t));
fs=input('Ingrese frecuencia de muestreo: ');
disp('CUANTIZACIÓN');
b=input('Ingrese la cantidad de bits de cuantización: ');
off=input('0 sin offset, 1 con offset: ');
# Señal "continua"
to=[0:0.01:100];
xo=function_handle(x)
xc=xo(to);
figure(1);subplot(2, 1, 1); plot(to, xo); title('Continua vs Discreta');
# Muestreo
T=1/fs; n=[0:1:100]; tm=n*T;
xd=xo(tm);
subplot(2,1,2); stem(tm, xd);
# Cuantización
L=2^b;
[xr, er, SNRr, xt, et, SNRt] = cuantizacion(x, L);
figure(2);subplot(2,1,1); stem(x, 'k'); xlabel('n'); ylabel('negro x[n], azul xr[n], rojo xt[n]');
hold on; stem(xr, 'b'); stem(xt, 'r');
subplot(2,1,2); stem(er); xlabel('n'); ylabel('azul er[n], rojo et[n]');
hold on; stem(et, 'r');
disp('SNRr: '); disp(SNRr);
disp('SNRt: '); disp(SNRt);
#Filtrado digital


