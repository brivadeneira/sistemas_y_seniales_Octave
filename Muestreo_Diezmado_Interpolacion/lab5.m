################################################
# Laboratorio 5 - Sistemas y Señales II - 2016 #
################################################
# INSTALACIONES PREVIAS
# sudo apt-get install liboctave-dev
# sudo apt-get install python-symp
#
# Programa principal
# ¡Cuidado! No se implementó control de variables de entrada.
close all; clc; clear all;
pkg load symbolic
pkg load signal
disp('MUESTREO');
syms t; x_str=input('Ingrese la señal de entrada x(t): '); xsym=sym(x_str); x=function_handle(xsym);
fs=input('Ingrese frecuencia de muestreo (en Hz): ');
disp('CUANTIZACIÓN');
tipo=input('Tipo, 0 redondeo, 1 truncamiento: ');
b=input('Ingrese la cantidad de bits de cuantización: ');
off=input('Modo, 0 sin offset, 1 con offset: ');
disp('DIEZMADO E INTERPOLACIÓN');
M=input('Ingrese factor de diezmado (M): ');
L=input('Ingrese factor de interpolación (L): ');
# Señal "continua"
to=[0:1/(100*fs):200/fs];
xo=x(to);
figure('name','Continua vs Discreta');
subplot(2, 2, 1); plot(to, xo); xlabel('t'); ylabel('x(t)');
subplot(2, 2, 3); stem(abs(fft(xo))); xlabel('w'); ylabel('|X(jw)|');
# Muestreo
T=1/fs; tm=[0:1/fs:200/fs];
xd=x(tm);
subplot(2, 2, 2); stem(tm, xd); xlabel('n'); ylabel('xd[n]=x(nT)');
subplot(2, 2, 4); stem(abs(fft(xd))); xlabel('w'); ylabel('|Xd(e^jw)|');
# Cuantización
Ln=2^b;
[xc, ec, SNRc] = cuantizacion(xd, Ln, tipo, off);
figure('name','Señal cuantizada'); 
subplot(3, 2, 1); stem(tm, xd); xlabel('n'); ylabel('xd[n]=x(nT)');
subplot(3, 2, 3); stem(abs(fft(xd))); xlabel('w'); ylabel('|Xd(e^jw)|');
subplot(3, 2 ,5); stem(ec); xlabel('n'); ylabel('ec[n]');
subplot(2, 2 ,2); stem(xc, 'b'); ylabel('xc[n]');
subplot(2, 2 ,4); stem(abs(fft(xc))); xlabel('w'); ylabel('|Xc[e^(jw)]|');
disp('SNR: '); disp(SNRc);
# Interpolación
if L>1 
	xe=exps(xc, L);
	figure('name','Salida del interpolador');
	subplot(2, 2, 1); stem(xc); xlabel('n'); ylabel('xc[n]');
	subplot(2, 2, 3); stem(abs(fft(xc))); xlabel('w'); ylabel('|Xc[e^(jw)]|');
	subplot(2, 2, 2); stem(xe); xlabel('n'); ylabel('xe[n]');
	subplot(2, 2, 4); stem(abs(fft(xe))); xlabel('w'); ylabel('|Xe[e^(jw)]|');
else
	xe=xc;
end
# Filtrado digital
wc=min(pi/L,pi/M);
[f,xu,xuf] = filtrado(xe, L, wc, 1024);
figure('name','Respuesta del filtro'); freqz (f, 1, []);
figure('name','Salida del filtro'); 
subplot(2, 2, 1); stem(xe); xlabel('n'); ylabel('xe[n]');
subplot(2, 2, 3); stem(abs(fft(xe))); xlabel('w'); ylabel('|Xe[e^(jw)]|');
subplot(2,2,2); stem(xu); xlabel('n'); ylabel('xfil[n]');
subplot(2,2,4); stem(abs(xuf)); xlabel('w'); ylabel('|Xfil[e^(jw)]|');
# Diezmado
xdz=dz(xu, M);
figure('name','Salida del diezmador');
subplot(2, 2, 1); stem(xu); xlabel('n'); ylabel('xfil[n]');
subplot(2, 2, 3); stem(abs(fft(xu))); xlabel('w'); ylabel('|Xfil[e^(jw)]|');
subplot(2, 2, 2); stem(xdz); xlabel('n'); ylabel('xdz[n]');
subplot(2, 2, 4); stem(abs(fft(xdz))); xlabel('w'); ylabel('|Xdz[e^(jw)]|');
