function [xt, et, SNRt] = tr(x, delta)
#Cuantización por truncamiento.
#Devuelve y grafica la señal cuantizada y el error de cuantización.
#Requiere a la señal muestreada (x) y tamaño del escalón de cuantización.
xt=(floor(x./delta)).*delta;
et=abs(x-xt);
SNRt=(sum(x.^2))/(sum(et.^2));
