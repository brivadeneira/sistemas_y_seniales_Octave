#Cuantización por redondeo o truncamiento.
#Devuelve la señal cuantizada y el error de cuantización.
#Requiere a la señal muestreada (x) y el número total de niveles de cuantización (L).
function [xc, ec, SNRc] = cuantizacion(x, L, tipo, off)
# 0 redondeo, 1 truncamiento
delta=(max(x)-min(x))/(L-1);
if tipo==0
	[xc, ec, SNRc] = rd(x, delta);
else
	[xc, ec, SNRc] = tr(x, delta);
end
# Offset
if off==1
	[xo] = offset(xc, L);
	xc=xo;
end

