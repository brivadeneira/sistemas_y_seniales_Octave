 **Laboratorio 5 - Sistemas y Señales II**
===================
Se simula un **sistema de procesamiento digital de señales** conformado por un bloque **interpolador** *(incrementa la frecuencia de muestreo, añade muestras)* y un bloque **diezmador** *(reduce la frecuencia de muestreo, elimina muestras)*, de una señal en tiempo discreto resultante de los procesos de **muestreo** y **cuantización** (con la posibilidad de elegir el modo entre redondeo y truncamiento).

Parámetros de entrada:
> - M (factor de diezmado).
*   L (factor de expansión).
*   Modo de cuantización (por redondeo, por truncamiento, con o sin offset).
*   b (cantidad de bit utilizados en la cuantización).
*   señal de entrada al sistema.
*   fs (frecuencia de muestreo).
