function [b,xu,xuf] = filtrado(x, L, wc, N);
wp=0.9*wc; ws=1.1*wc;
[M, w, Beta, ftype] = kaiserord ([wp/(2*pi), ws/(2*pi)], [1, 0], [0.01, 0.01]);
b = fir1 (M, w, kaiser (M+1, Beta), ftype, "noscale");
xu=filter(b, 1, x);
xuf=fft(xu, N);

