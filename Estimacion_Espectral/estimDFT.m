function [xu,xuf] = estimDFT(x, fs, beta, L, N);
# Recibe:
# x=secuencia a enventanar (vector)
# beta=parámetro de la ventana
# L longitud de la ventana
# N cantidad de muestras de la DFT
#
# Retorna:
# xu Secuencia enventanada
#
#coeficientes de la ventana
u=kaiser(L,beta);
#enventanado
x2=([1:L]);
x2.*u;
xuf=fft(xu,N);
#ploteo
figure(1); subplot(3, 1, 1); stem(x); subplot(3, 1, 2); stem(u); subplot(3, 1, 3); stem(xu);
figure(2); subplot(3, 1,1);stem(abs(fft(xu,N)));
k=0:(2*pi)/1023:2*pi;
subplot(3, 1,2); plot(k, abs(xuf));
k2=k*fs/2*pi;
subplot(3, 1,3); plot(k2, abs(xuf));
xu2=ifft(xuf);
figure(3); stem(xu2); 
