#Dados los vectores c y p que contienen los ceros y polos de la función transferencia de un filtro digital, se realiza la gráfica en magnitud y fase de la respuesta en frecuencia del mismo.
function [H, w] = plotfiltro(c, p)
num=[1,-c(length(c))];
if length(c)>1
	for i=length(c)-1:-1:1
		num=conv([1,-c(i)],num);
	end
end
den=[1,-p(length(p))];
if length(p)>1 
	for i=length(p)-1:-1:1
		den=conv([1,-p(i)],den);
	end
end
[H,w]=freqz(num, den);
freqz(num,den);
