# Diseño de un filtro FIR digitalmediante elmétodo dela ventana de Kaiser.
# Requerimientos
# wp, Frec de corte de la banda de PASO
# ws, Frec de corte de la banda de RECHAZO
# delta, error de aproximación pico

# Retorna:
# Longitud de la respuesta al impulso, parámetro beta.
# Gráfico de la respuesta en frecuencia.
# Gráfico de la respuesta impulsional.
function [L, beta] = firkaiser(wp, ws, delta);
# Parámetros de la ventana de Kaiser
#dw=ws-wp;
#A=-20*log10(delta);

# Beta
# beta=0.1102*(A-8.7); A>50
# beta=0.5842*(A-21)^0.4+0.07886*(A-21); 21 <= A <= 50
# beta=0;	A<21
	
# Longitud de la respuesta al impulso
#M=(A-8)/(2.285*dw);


# kaiserord
# Recibe:
# F (vector): banda de frecuencias.
# A (vector): Amplitudes deseadas.
# delta: Rizado de las bandas.
# Retorna:
# M: orden de filtro
# wn (vector): banda requerida por el comando fir1
# beta

F=[wp/(2*pi), ws/(2*pi)];
A=[1, 0];
R=[delta, delta];

[M, W, beta, ftype] = kaiserord (F, A, delta);
M=round(M);
f = fir1(M, W, kaiser(M+1, beta), ftype, "noscale");
figure(1); freqz (f, 1, []);
figure(2); impz(f, 1, []);
L=M+1;

#Ejemplo help kaiserord
#[n, w, beta, ftype] = kaiserord ([1000, 1200], [1, 0], [0.05, 0.05], 11025);
#b = fir1 (n, w, kaiser (n+1, beta), ftype, "noscale");
#freqz (b, 1, [], 11025);
