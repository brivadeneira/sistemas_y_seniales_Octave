# Sistemas y señales en GNU/Octave

Se automatizan tareas como:

* Diezmado.
* REpansión.
* Cuantización
  * por redondeo
  * por truncamiento 
  * cálculo error de cuantización
* Determinación de la respuesta en frecuencia de un filtro digital mediante sus polos y ceros.

... proveyendo en todos los casos gráficas de las señales involucradas.
