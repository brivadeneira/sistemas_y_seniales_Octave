// Polos y ganancia de la fcion transferencia
[pols,gain]=zpbutt(4,2*%pi*563.365);
// Fcion transferencia filtro analogico
Hs=gain/real(poly(pols,'s'));
hs=syslin('c',Hs);
bode(hs);
T=1;
z = poly(0,'z');
// Respuesta en frecuencia filtro digitral
// Bilineal
Hz = horner(h,(2/T)*((z-1)/(z+1)));
hz=syslin('c',Hz);
// Coeficientes a vector
numz = coeff(Hz(2));
denz = coeff(Hz(3));
numz = numz(length(numz):-1:1);
denz = denz(length(denz):-1:1); 

//Senal audible a vector
[suma.values, fs, nbits]=wavread('suma.wav');
suma.values=suma.values';
tam=length(suma.values);
suma.time=[0:1/(fs-1):1]';
//Reproducir senal audible desde el vector
sound(suma.values);

